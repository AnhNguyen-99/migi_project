package com.migi.migi_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiGiProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiGiProjectApplication.class, args);
    }

}
